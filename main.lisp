(defpackage :static-program-op
  (:use :cl)
  (:export #:main))

(in-package :static-program-op)

(uiop:register-image-dump-hook
 (lambda ()
   (dolist (lib (cffi:list-foreign-libraries))
     (when (eql (cffi:foreign-library-type lib) :grovel-wrapper)
       (cffi:close-foreign-library lib)))))

(defun main ()
  (let ((db (merge-pathnames #p"test.db" uiop:*temporary-directory*)))
    ;; this goes through the shared library
    (sqlite:connect db)
    ;; this was defined by grovel
    (format t "~a~%" osicat-posix:sigint)
    ;; this was defined by a wrapper file
    (format t "~a~%" (osicat-posix:lstat "/tmp"))))
