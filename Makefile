static-program-op: $(wildcard *.lisp)
	sbcl \
		--load ~/quicklisp/setup.lisp \
		--eval '(ql:quickload :cffi-grovel)' \
		--eval '(ql:quickload :static-program-op)' \
		--eval '(asdf:make :static-program-op)' \
		--quit
