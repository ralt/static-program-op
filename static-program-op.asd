(defsystem "static-program-op"
  :description "Minimal test code for static-program-op"
  :license "MIT"
  :defsystem-depends-on ("cffi-grovel")
  :depends-on (;; osicat has some wrapper/grovel files
	       :osicat
	       ;; sqlite relies on a system shared library
	       :sqlite)
  :components ((:file "main"))
  :build-operation :static-program-op
  :build-pathname "static-program-op"
  :entry-point "static-program-op:main")
